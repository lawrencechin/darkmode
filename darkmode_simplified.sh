#!/bin/bash
#
## macOS Dark Mode at sunset
## Solar times pulled from Night Shift
## Author: Lord Chang
## Version 1.0

## Global variables ##
darkdir=~/Library/Application\ Support/darkmode # darkmode directory
plistR=com.legacychang.darkmode.sunrise
plistS=com.legacychang.darkmode.sunset
plistRPath=~/Library/LaunchAgents/"$plistR".plist
plistSPath=~/Library/LaunchAgents/"$plistS".plist
log_file=~/Library/Logs/com.legacychang.darkmode.log

## Functions ##

# Set Kitty theme - Random theme based on light or dark mode
kitty_theme(){
    kitty_path=/Applications/kitty.app/Contents/MacOS/kitty
    kitty_light_array=("Akihabara South Exit" "Fresh Air" "Fresh Air Original" "Himitsu House Day" "Japanese Garden Light" "Riverside Day" "School Entry Light" "Subway Seoul" "Tokyo Street" "Tokyo Street Sunset Light")

    kitty_dark_array=("Akihabara South Exit Night" "Crisp Night Lighter" "Crisp Night" "Dracula Modified" "Himitsu House Night" "Japanese Garden Dark" "Old Bar" "Riverside Night" "School Entry Dark" "Sky City" "Subway Seoul Dark" "Tokyo Street Night" "Tokyo Street Sunset Night" "Ultima Bar" "Wild Night Club")
    kitty_light_theme="${kitty_light_array[RANDOM%${#kitty_light_array[@]}]}"
    kitty_dark_theme="${kitty_dark_array[RANDOM%${#kitty_dark_array[@]}]}"
    # $1 light | dark
    if [ -e "$kitty_path" ]; then
        case $1 in 
            light)
                `"$kitty_path" +kitten themes --reload-in=all "$kitty_light_theme"`
                ;;
            dark)
                `"$kitty_path" +kitten themes --reload-in=all "$kitty_dark_theme"`
                ;;
        esac
    fi
}

# Set dark mode - Sunrise = off Sunset = on
# Version using Shortcuts - seems to be flaky
darkModeShortcuts() {
	case $1 in
		off)
            shortcuts run "Lightmode"
			;;
		on)
            shortcuts run "Darkmode"
			;;
	esac
}

# Set dark mode - Sunrise = off Sunset = on
# Version using OSAscript - seems more robust 
darkModeOSA(){
    case $1 in
        off)
            osascript -e '
                tell application id "com.apple.systemevents"
                    launch
                    delay 2
                end tell
                tell application id "com.apple.systemevents"
                    tell appearance preferences
                        if dark mode is true then
                            set dark mode to false
                        end if
                    end tell
                end tell
            '
            ;;
        on)
            osascript -e '
                tell application id "com.apple.systemevents"
                    launch
                    delay 2
                end tell
                tell application id "com.apple.systemevents"
                    tell appearance preferences
                        if dark mode is false then
                            set dark mode to true
                        end if
                    end tell
                end tell
            '
            ;;
    esac
}

# Get time and Format to be Usable
getTime() {
	riseT=$(/usr/libexec/corebrightnessdiag nightshift-internal | grep nextSunrise | cut -d \" -f2)
	setT=$(/usr/libexec/corebrightnessdiag nightshift-internal | grep nextSunset | cut -d \" -f2)
	# Test for 12 hour format
	if [[ $riseT == *M* ]] || [[ $setT == *M* ]]; then
		formatT="%Y-%m-%d %H:%M:%S %p %z"
	else
		formatT="%Y-%m-%d %H:%M:%S %z"
	fi

	# Convert to local time
	riseTL=$(date -jf "$formatT" "$riseT" +"%H:%M")
	setTL=$(date -jf "$formatT" "$setT" +"%H:%M")
	riseH=$(date -jf "$formatT" "$riseT" +"%H" | sed 's/^0//')
	riseM=$(date -jf "$formatT" "$riseT" +"%M" | sed 's/^0//')
	setH=$(date -jf "$formatT" "$setT" +"%H" | sed 's/^0//')
	setM=$(date -jf "$formatT" "$setT" +"%M" | sed 's/^0//')

	# Get current 24H time hr and min
	timeH=$(date +"%H" | sed 's/^0//')
	timeM=$(date +"%M" | sed 's/^0//')

	# Convert times to total min
	riseMin=$(($riseH * 60 + $riseM))
	setMin=$(($setH * 60 + $setM))
	nowMin=$(($timeH * 60 + $timeM))

	# Log
	printToLog "Sunrise: ""$riseTL"" Sunset: ""$setTL"
}

# Create both sunrise and sunset plists without calendarinterval
createPlist() {
	mkdir -p ~/Library/LaunchAgents; cd "$_" || return # Create LaunchAgents directory (if required) and cd there
	# Setup launch agent plists
	/usr/libexec/PlistBuddy -c "Add :Label string "$plistR"" "$plistRPath" 1> /dev/null
	/usr/libexec/PlistBuddy -c "Add :Label string "$plistS"" "$plistSPath" 1> /dev/null
    /usr/libexec/PlistBuddy -c "Add :Program string ${darkdir}/darkmode_simplified.sh" "$plistRPath"
    /usr/libexec/PlistBuddy -c "Add :Program string ${darkdir}/darkmode_simplified.sh" "$plistSPath"
}

# Add or Update plists with calendarintervals
editPlist() {
    # $1 add|update $2 hour $3 minute $4 plistPath
	case $1 in
		add)
			/usr/libexec/PlistBuddy -c "Add :StartCalendarInterval:Hour integer $2" "$4"
			/usr/libexec/PlistBuddy -c "Add :StartCalendarInterval:Minute integer $3" "$4"
			;;
		update)
			/usr/libexec/PlistBuddy -c "Set :StartCalendarInterval:Hour $2" "$4"
			/usr/libexec/PlistBuddy -c "Set :StartCalendarInterval:Minute $3" "$4"
			;;
	esac
}

# Bootout, edit plist, Bootstrap
## nb. This function is the reason there are two plists rather 
## than one. Try as I might, one can't simply reload a modified
## plist and get the changes to override the cached version 
## without booting out/unloading.
## When this happens, the script stops running and the plist
## never loads again. 
updateOrModifyPlist(){
    # $1 rise/set Hour $2 rise/set Minute $3 plistPath
    hourExists=$(/usr/libexec/PlistBuddy -c 'print ":StartCalendarInterval:Hour"' "$3" 2>/dev/null)
    exitCode=$?

    unloadPlist $3

    if (( exitCode == 0 )); then
        editPlist "update" $1 $2 $3
        printToLog "Updating plist…"
    else
        editPlist "add" $1 $2 $3
        printToLog "Adding to plist…"
    fi

    loadPlist $3
}

loadPlist(){
    loadPlist=$(launchctl bootstrap gui/501/ "$1")
    loadExitCode=$?
    printToLog "Loading Service | Error ($loadExitCode)"
}

unloadPlist(){
    # $1 plistPath
    bootPlist=$(launchctl bootout gui/501/ "$1")
    bootPlistExitCode=$?
    printToLog "Unloading Service | Error($bootPlistExitCode)"
    sleep 5 # wait for unload to finish, probably unnecessary
}

# Uninstall
unstl() {
	# Unload launch agents
    unloadPlist "$plistRPath"
    unloadPlist "$plistSPath"
	# Check if darkmode files exist and move to Trash
	if [ -d "$darkdir" ]; then
        time=$(date +%H%M%S)
        mv "$darkdir" "$(dirname "$darkdir")"/darkmode_"$time" # Add date to darkdir
        darkdird=$(find "$(dirname "$darkdir")"/darkmode_"$time" -type d)
        mv "$darkdird" ~/.Trash
	fi
	if [ -f "$plistRPath" ] || [ -f "$plistSPath" ]; then
		mv "$plistRPath" ~/.Trash
        mv "$plistSPath" ~/.Trash
	fi
	if [ -f "$log_file" ]; then
		mv "$log_file" ~/.Trash
	fi
}

# Error logging
printToLog(){
    monkey_array=("🙈" "🐵" "🙉" "🙊")
    echo "$( date +"%D %T" ) ${monkey_array[RANDOM%${#monkey_array[@]}]} > $1" >> "$log_file"
}

log() {
	while IFS='' read -r line; do
		echo "$(date +"%D %T") 🙈: $line" >> "$log_file" 
	done
}

# Determine if now is pre or post sunset/sunrise
time_comparison(){
    getTime
    if [[ $nowMin -lt $riseMin || $nowMin -ge $setMin ]]; then
        # Sunset
        updateOrModifyPlist "$riseH" "$riseM" "$plistRPath"
        darkModeOSA "on"
        kitty_theme "dark"
    else
        # Sunrise
        updateOrModifyPlist "$setH" "$setM" "$plistSPath"
        darkModeOSA "off"
        kitty_theme "light"
    fi
}

# Move files into place for the first launch
firstTimeLaunch(){
    if [ ! -d "$darkdir" ]; then
        shdir="$(cd "$(dirname "$0")" && pwd)" # Get script path
        mkdir "$darkdir"
        cp -p "$shdir"/darkmode_simplified.sh "$darkdir"/ # Copy script to darkmode directory
        if [ ! -f "$plistRPath" ] || [ ! -f "$plistSPath" ]; then
            createPlist
        fi
    fi

    time_comparison
}

# Error log
exec 2> >(log) # not sure where this should go…

# Run the darn script
# Uninstall switch
if [ "$1" == '-u' ]; then
	unstl
	error=$? # Get exit code from unstl()
	if [ $error -ne 0 ]; then # If exit code not equal to 0
		echo "🙈 > Uninstall failed!"
		exit $error
	fi
	echo "🐵 > Uninstall successful."
	echo "🐵 > Darkmode files have been sent to your Trash."
	exit 0
else
    firstTimeLaunch
fi
