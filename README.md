# Darkmode

Automatically change **macOS** system appearance at sunrise/sunset.

![Header Image](./resources/imgs/deux_6.jpg)

## What does it do?
Runs a script to change system appearance at sunset and sunrise based on the **Nightshift** schedule. Also used to change to a random dark/light [**Kitty**](https://sw.kovidgoyal.net/kitty/) theme. Forked from [katernet/darkmode](https://github.com/katernet/darkmode).

## How does it work?
The shell script probes `nightshift-internal` to grab sunrise/sunset times which are in turn are used to create plists that run at those two specific times using `launchd`.

## Why?
Modern **macOS** has an 'auto' mode to switch between dark and light mode but this function doesn't work on unsupported systems ([OpenCore Legacy Patcher](https://dortania.github.io/OpenCore-Legacy-Patcher/)). OS' prior to **Catalina** do not have this auto mode. In addition, system appearance isn't the only thing that might change at sunrise/sunset: adapt the script to run any commands you see fit at these times.

## Usage

``` sh
# Install
$ ./darkmode_simplified.sh
# Uninstall
$ ./darkmode_simplified.sh -u

# If you prefer to use the older version
# Install
$ ./darkmode.sh
# Or
$ ./darkmode.sh HHMM HHMM # Set own schedule
# Uninstall
$ ./darkmode.sh /u
```

## Requirements
- **Nightshift** must be enabled in conjunction with sunrise/sunset schedule
- In order to switch system appearance using **Shortcuts**, you must be using macOS Monterey 12.0+. Shortcut files available in the `resources` folder
- [**Kitty**](https://sw.kovidgoyal.net/kitty/) installed in `/Applications` to use the **Kitty** theme switcher. To use the themes in the script you can grab them from the following repositories:
    - [Fresh Air](https://gitlab.com/lawrencechin/fresh_air.nvim)
    - [Arsenixc Themes](https://gitlab.com/lawrencechin/arsenixc_themes.nvim)

### Note

![The Black Cat](./resources/thumbs/the_black_cat.png)

Should you wish your **Kitty** theme and [**Neovim**](https://neovim.io) theme to match then visit [**Dark-Notify**](https://github.com/cormacrelf/dark-notify), install then add the following (or modify) to your **Neovim** configs:

``` lua
function current_kitty_theme()
    local kitty_theme_name = vim.fn.system( "rg BEGIN_KITTY -A 1 -o ~/.config/kitty/kitty.conf | tail -1 | sed 's/# //' | tr -d '\n'" ) 
    local fresh_air = "fresh_air.functions"
    local arsenixc = "arsenixc.functions"
    local fresh_air_themes = require ( fresh_air ).list_themes()
    local arsenixc_themes = require ( arsenixc ).list_themes()

    if vim.api.nvim_get_vvar( "shell_error" ) ~= 0 then
        -- couldn't get kitty theme name
        return { colour = fresh_air, light = "fresh_air", dark = "crisp_night" }
    else
        local neovim_theme_name = string.gsub( string.lower( kitty_theme_name ), " ", "_" )
        print( neovim_theme_name )
        if neovim_theme_name == "dracula_modified" then
            return { colour = fresh_air, light = "crisp_night", dark = "crisp_night" }
        elseif neovim_theme_name == "riverside_night_light" then
            return { colour = arsenixc, light = "riverside_nightlight", dark = "riverside_nightlight" }
        else
            for _, theme in pairs( fresh_air_themes ) do
                if theme == neovim_theme_name then
                    return { colour = fresh_air, light = neovim_theme_name, dark = neovim_theme_name }
                end
            end

            for  _,theme in pairs( arsenixc_themes ) do
                if theme == neovim_theme_name then
                    return { colour = arsenixc, light = neovim_theme_name, dark = neovim_theme_name }
                end
            end
            -- theme not found, return default light and dark theme
            return { colour = fresh_air, light = "fresh_air", dark = "crisp_night" }
        end
    end
end

require( "dark_notify" ).run({
    onchange = function( mode )
        local theme = current_kitty_theme()
        if mode == "dark" then
            require( theme.colour ).change_style( theme.dark )
        else
            require( theme.colour ).change_style( theme.light )
        end
    end,
})
```

Bear in mind that this function largely works because the themes have the same name. You may need to change it quite a bit for your own specific setup. 

## Changes from Original Script
- Removed static time arguments (**Nightshift** only)
- No longer uses a database to store data
- Modifies sunrise/sunset times everyday rather than on each Monday
- Removed WIFI check
- Removed **Alfred** settings: no longer needed and I don't use **Alfred**
- Removed OS check as it breaks on 11.0+
- Added random **Kitty** themes - other terminal emulators may have their own methods to change theme on dark/light mode switch (true of **iTerm** for example)
- Added method to change system appearance using **Shortcuts** though it seems to not function entirely, not currently enabled
- Updated all `launchctl` calls to use modern methods such as `bootstrap` and `bootout` rather than `load` and `unload`
- Removed `runAtLoad` option in the sunrise plist as it isn't needed, runs the script at wrong times and changes the **Kitty** theme each time
- Changed the uninstall argument from `/u` to `-u`
- Slightly more verbose logging and many more monkeys… 🙈

## Design
All the files used to make the header image. I'd recommend opening links in a new tab.

### Typography

[![Initial Sketches](./resources/thumbs/type_sketches.jpg)](./resources/imgs/type_sketches.jpg)
[![Dark Typography Explorations](./resources/thumbs/type_dark.jpg)](./resources/imgs/type_dark.png)
[![Dark Typography Expanded](./resources/thumbs/type_dark_variations.jpg)](./resources/imgs/type_dark_variations.png)
[![Light Typography Explorations](./resources/thumbs/type_light.jpg)](./resources/imgs/type_light.png)
[![Light Typography Expanded](./resources/thumbs/type_light_variations.jpg)](./resources/imgs/type_light_variations.png)
[![Typography Final Groups](./resources/thumbs/type_final_group.jpg)](./resources/imgs/type_final_group.png)

### Shapes
[![Interior Shapes](./resources/thumbs/shapes_cutouts.jpg)](./resources/imgs/shapes_cutouts.png)
[![Shapes](./resources/thumbs/shapes.jpg)](./resources/imgs/shapes.png)
[![Shapes and Interiors Combined](./resources/thumbs/shapes_with_cutouts.jpg)](./resources/imgs/shapes_with_cutouts.png)
[![Shape Shadows and No Outlines](./resources/thumbs/shapes_shadows.jpg)](./resources/imgs/shapes_shadows.png)
[![Designs](./resources/thumbs/designs.jpg)](./resources/imgs/designs.png)
[![Final Designs](./resources/thumbs/designs_final_1.jpg)](./resources/imgs/designs_final.png)

### Backgrounds

All artworks used taken from [Artvee](https://artvee.com/).

#### Arnold Wiltz - The Causeway

[![The Causeway 1](./resources/thumbs/causeway_0.jpg)](./resources/imgs/causeway_0.jpg)
[![The Causeway 2](./resources/thumbs/causeway_1.jpg)](./resources/imgs/causeway_1.jpg)
[![The Causeway 3](./resources/thumbs/causeway_2.jpg)](./resources/imgs/causeway_2.jpg)
[![The Causeway 4](./resources/thumbs/causeway_3.jpg)](./resources/imgs/causeway_3.jpg)
[![The Causeway 5](./resources/thumbs/causeway_4.jpg)](./resources/imgs/causeway_4.jpg)
[![The Causeway 6](./resources/thumbs/causeway_5.jpg)](./resources/imgs/causeway_5.jpg)
[![The Causeway 7](./resources/thumbs/causeway_6.jpg)](./resources/imgs/causeway_6.jpg)
[![The Causeway 8](./resources/thumbs/causeway_7.jpg)](./resources/imgs/causeway_7.jpg)

#### Arnold Wiltz - Marsury verses Sky

[![Marsury Versus Sky 1](./resources/thumbs/marsury_0.jpg)](./resources/imgs/marsury_0.jpg)
[![Marsury Versus Sky 2](./resources/thumbs/marsury_1.jpg)](./resources/imgs/marsury_1.jpg)
[![Marsury Versus Sky 3](./resources/thumbs/marsury_2.jpg)](./resources/imgs/marsury_2.jpg)
[![Marsury Versus Sky 4](./resources/thumbs/marsury_3.jpg)](./resources/imgs/marsury_3.jpg)
[![Marsury Versus Sky 5](./resources/thumbs/marsury_4.jpg)](./resources/imgs/marsury_4.jpg)
[![Marsury Versus Sky 6](./resources/thumbs/marsury_5.jpg)](./resources/imgs/marsury_5.jpg)
[![Marsury Versus Sky 7](./resources/thumbs/marsury_6.jpg)](./resources/imgs/marsury_6.jpg)
[![Marsury Versus Sky 8](./resources/thumbs/marsury_7.jpg)](./resources/imgs/marsury_7.jpg)
[![Marsury Versus Sky 9](./resources/thumbs/marsury_8.jpg)](./resources/imgs/marsury_8.jpg)
[![Marsury Versus Sky 10](./resources/thumbs/marsury_9.jpg)](./resources/imgs/marsury_9.jpg)

#### Wassily Kandinsky - Deux Côtes

[![Deux Côtes 1](./resources/thumbs/deux_0.jpg)](./resources/imgs/deux_0.jpg)
[![Deux Côtes 2](./resources/thumbs/deux_1.jpg)](./resources/imgs/deux_1.jpg)
[![Deux Côtes 3](./resources/thumbs/deux_2.jpg)](./resources/imgs/deux_2.jpg)
[![Deux Côtes 4](./resources/thumbs/deux_3.jpg)](./resources/imgs/deux_3.jpg)
[![Deux Côtes 5](./resources/thumbs/deux_4.jpg)](./resources/imgs/deux_4.jpg)
[![Deux Côtes 6](./resources/thumbs/deux_5.jpg)](./resources/imgs/deux_5.jpg)
[![Deux Côtes 7](./resources/thumbs/deux_6.jpg)](./resources/imgs/deux_6.jpg)
[![Deux Côtes 8](./resources/thumbs/deux_7.jpg)](./resources/imgs/deux_7.jpg)
[![Deux Côtes 9](./resources/thumbs/deux_8.jpg)](./resources/imgs/deux_8.jpg)

#### Wassily Kandinsky - Dunkle Seiten

[![Dunkle Seiten 1](./resources/thumbs/dunkle_0.jpg)](./resources/imgs/dunkle_0.jpg)
[![Dunkle Seiten 2](./resources/thumbs/dunkle_1.jpg)](./resources/imgs/dunkle_1.jpg)
[![Dunkle Seiten 3](./resources/thumbs/dunkle_2.jpg)](./resources/imgs/dunkle_2.jpg)
[![Dunkle Seiten 4](./resources/thumbs/dunkle_3.jpg)](./resources/imgs/dunkle_3.jpg)
[![Dunkle Seiten 5](./resources/thumbs/dunkle_4.jpg)](./resources/imgs/dunkle_4.jpg)
[![Dunkle Seiten 6](./resources/thumbs/dunkle_5.jpg)](./resources/imgs/dunkle_5.jpg)
[![Dunkle Seiten 7](./resources/thumbs/dunkle_6.jpg)](./resources/imgs/dunkle_6.jpg)
[![Dunkle Seiten 8](./resources/thumbs/dunkle_7.jpg)](./resources/imgs/dunkle_7.jpg)
[![Dunkle Seiten 9](./resources/thumbs/dunkle_8.jpg)](./resources/imgs/dunkle_8.jpg)
[![Dunkle Seiten 10](./resources/thumbs/dunkle_9.jpg)](./resources/imgs/dunkle_9.jpg)
[![Dunkle Seiten 11](./resources/thumbs/dunkle_10.jpg)](./resources/imgs/dunkle_10.jpg)
[![Dunkle Seiten 12](./resources/thumbs/dunkle_11.jpg)](./resources/imgs/dunkle_11.jpg)
[![Dunkle Seiten 13](./resources/thumbs/dunkle_12.jpg)](./resources/imgs/dunkle_12.jpg)
[![Dunkle Seiten 14](./resources/thumbs/dunkle_13.jpg)](./resources/imgs/dunkle_13.jpg)
